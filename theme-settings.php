<?php

/**
 * @file
 * Implements().
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

/**
 * @file
 * novel_delights theme file.
 */

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function novel_delights_form_system_theme_settings_alter(&$form, FormStateInterface $form_state) {
  if ($form['#attributes']['class'][0] == 'system-theme-settings') {
    $form['#attached']['library'][] = 'novel_delights/theme.setting';

    $form['cafe_house_info'] = [
      '#markup' => '<h2><br/>Advanced Theme Settings</h2><div class="messages messages--warning">Clear cache after making any changes in theme settings. <a href="../../config/development/performance">Click here to clear cache</a></div>',
    ];
    //header logo content section
    $form['header_logo'] = [
        '#type' => 'details',
        '#title' => t('Header Logo Section'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
    ];
    $form['header_logo']['select_header_logo'] = [
        '#type' => 'checkbox',
        '#title' => t('You want to add header logo text'),
        '#default_value' => theme_get_setting('select_header_logo'),
    ];
    $display = theme_get_setting('select_header_logo');
    if ($display){
         $form['header_logo']['header_logo_text'] = [
        '#type' => 'textfield',
        '#title' => t('Add header logo text'),
        '#default_value' => theme_get_setting('header_logo_text'),
      ];
    }
        //header below content section

        $form['header_below_content_section'] = [
            '#type' => 'details',
            '#title' => t('Header below conntent section'),
        ];
        $form['header_below_content_section']['select_header_below_content_section'] = [
            '#type' => 'checkbox',
            '#title' => t('You want to display header below content section'),
            '#default_value' => theme_get_setting('select_header_below_content_section'),
        ];
        define("image-value", 3);
        $visible = theme_get_setting('select_header_below_content_section');
        if($visible){
            $form['header_below_content_section']['header_below_content_section_image'] = [
                '#type' => 'details',
                '#title' => t('Images section'),
            ];
            $form['header_below_content_section']['header_below_content_section_image']['select_header_below_content_section_image'] = [
                '#type' => 'checkbox',
                '#title' => t('You want to display image section'),
                '#default_value' => theme_get_setting('select_header_below_content_section_image'),
            ];
            $visible = theme_get_setting('select_header_below_content_section_image');
            if($visible){
            for($i=1; $i<=3; $i++){
            $form['header_below_content_section']['header_below_content_section_image']['img_1'.$i] = [
                '#type' => 'managed_file',
                '#title' => t('You can put your desire image_'.$i),
                '#default_value' => theme_get_setting('img_1'.$i),
                '#upload_location' => 'public://',
                '#upload_validators' => [
                   'file_validate_extensions' => ['gif png jpg jpeg svg'], 
                ]
            ];
            $content_image1 = theme_get_setting('img_1'.$i);
            if (!empty($content_image1)) {
                $file = File::load($content_image1[0]);
                $file->setPermanent();
                $file->save();
                $file_usage = \Drupal::service('file.usage')  ;
                $file_usage_check = $file_usage->listUsage($file);
                if (empty($file_usage_check)) {
                    $file_usage->add($file, 'novel_delights', 'theme', $content_image1[0]);
                }
            }
        }

        }
        $form['header_below_content_section']['content_left_image_section'] = [
            '#type' => 'details',
            '#title' => t('Left image section of title'),
        ];
        $form['header_below_content_section']['content_left_image_section']['select_content_left_image_section'] = [
            '#type' => 'checkbox',
            '#title' => t('You want to display left image section of title'),
            '#default_value' => theme_get_setting('select_content_left_image_section'),
        ];
        $visible = theme_get_setting('select_content_left_image_section');
        if($visible){
        $form['header_below_content_section']['content_left_image_section']['title_left_img'] = [
            '#type' => 'managed_file',
            '#title' => t('You can put image left side of title'),
            '#default_value' => theme_get_setting('title_left_img'),
            '#upload_location' => 'public://',
            '#upload_validators' => [
                'file_validate_extensions' => ['gif png jpg jpeg svg'],
            ]
        ];
        $content_image1 = theme_get_setting('title_left_img');
            if (!empty($content_image1)) {
                $file = File::load($content_image1[0]);
                $file->setPermanent();
                $file->save();
                $file_usage = \Drupal::service('file.usage')  ;
                $file_usage_check = $file_usage->listUsage($file);
                if (empty($file_usage_check)) {
                    $file_usage->add($file, 'novel_delights', 'theme', $content_image1[0]);
                }
            }
        }
             // ----------------- middle title------------
        $form['header_below_content_section']['header_below_title'] = [
            '#type' => 'details',
            '#title' => t('Title section'),
        ];

        $form['header_below_content_section']['header_below_title']['select_header_below_title'] = [
            '#type' => 'checkbox',
            '#title' => t('You want to display title section'),
            '#default_value' => theme_get_setting('select_header_below_title'),
        ];
        $visible = theme_get_setting('select_header_below_title');
        if($visible){
        $form['header_below_content_section']['header_below_title']['header_below_title_name'] = [
            '#type' => 'textfield',
            '#title' => t('You can add title'),
            '#default_value' => theme_get_setting('header_below_title_name'),
        ];
        }
        $form['header_below_content_section']['content_right_image_section'] = [
            '#type' => 'details',
            '#title' => t('Right image section of title'),
        ];
        $form['header_below_content_section']['content_right_image_section']['select_content_right_image_section'] = [
            '#type' => 'checkbox',
            '#title' => t('You want to display right image section of title'),
            '#default_value' => theme_get_setting('select_content_right_image_section'),
        ];
        $visible = theme_get_setting('select_content_right_image_section');
        if($visible){
            // ----------------- right side img------------
            $form['header_below_content_section']['content_right_image_section']['title_right_img'] = [
                '#type' => 'managed_file',
                '#title' => t('You can put image right side of title'),
                '#default_value' => theme_get_setting('title_right_img'),
                '#upload_location' => 'public://',
                '#upload_validators' => [
                    'file_validate_extensions' => ['gif png jpg jpeg svg'],
                ]
            ];
            $content_image1 = theme_get_setting('title_right_img');
                if (!empty($content_image1)) {
                    $file = File::load($content_image1[0]);
                    $file->setPermanent();
                    $file->save();
                    $file_usage = \Drupal::service('file.usage')  ;
                    $file_usage_check = $file_usage->listUsage($file);
                    if (empty($file_usage_check)) {
                        $file_usage->add($file, 'novel_delights', 'theme', $content_image1[0]);
                    }
                }
        }
        // --------------------subtitle----------
        $form['header_below_content_section']['header_below_subtitle'] = [
            '#type' => 'details',
            '#title' => t('Subtitle section'),
        ];

        $form['header_below_content_section']['header_below_subtitle']['select_header_below_subtitle'] = [
            '#type' => 'checkbox',
            '#title' => t('You want to display subtitle section'),
            '#default_value' => theme_get_setting('select_header_below_subtitle'),
        ];
        $visible = theme_get_setting('select_header_below_subtitle');
        if($visible){
        $form['header_below_content_section']['header_below_subtitle']['header_below_subtitle_name'] = [
            '#type' => 'textfield',
            '#title' => t('You can add subtitle'),
            '#default_value' => theme_get_setting('header_below_subtitle_name'),
        ];
        }
        // --------------------Description----------
        $form['header_below_content_section']['header_below_description'] = [
            '#type' => 'details',
            '#title' => t('Description section'),
        ];
         
        $form['header_below_content_section']['header_below_description']['select_header_below_description'] = [
            '#type' => 'checkbox',
            '#title' => t('You want to display description section'),
            '#default_value' => theme_get_setting('select_header_below_description'),
        ];
        $visible = theme_get_setting('select_header_below_description');
        if($visible){
        $form['header_below_content_section']['header_below_description']['header_below_description_value'] = [
            '#type' => 'textarea',
            '#title' => t('You can add description'),
            '#default_value' => theme_get_setting('header_below_description_value'),
        ];
        }
        // --------------------Button value----------
        $form['header_below_content_section']['header_below_button'] = [
            '#type' => 'details',
            '#title' => t('Button section'),
        ];
         
        $form['header_below_content_section']['header_below_button']['select_header_below_button'] = [
            '#type' => 'checkbox',
            '#title' => t('You want to display button section'),
            '#default_value' => theme_get_setting('select_header_below_button'),
        ];
        $visible = theme_get_setting('select_header_below_button');
        if($visible){
        $form['header_below_content_section']['header_below_button']['header_below_button_value'] = [
            '#type' => 'textfield',
            '#title' => t('You can add button'),
            '#default_value' => theme_get_setting('header_below_button_value'),
        ];
        
        }
    }
       // Footer Middle Section
    $form['footer_middle_section'] = [
        '#type' => 'details',
        '#title' => t('Footer middle section'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      ];
    $form['footer_middle_section']['select_footer_middle_section'] = [
        '#type' => 'checkbox',
        '#title' => t('You want to add footer middle section'),
        '#default_value' => theme_get_setting('select_footer_middle_section'),
    ];
    $visible = theme_get_setting('select_footer_middle_section');
    if($visible){
        $form['footer_middle_section']['footer_middle_section_title'] = [
            '#type' => 'details',
            '#title' => t('Title section'),
        ];
         
        $form['footer_middle_section']['footer_middle_section_title']['select_footer_middle_section_title'] = [
            '#type' => 'checkbox',
            '#title' => t('You want to display title'),
            '#default_value' => theme_get_setting('select_footer_middle_section_title'),
        ];
        $visible = theme_get_setting('select_footer_middle_section_title');
        if($visible){
        $form['footer_middle_section']['footer_middle_section_title']['footer_middle_section_title_name'] = [
        '#type' => 'textfield',
        '#title' => t('You can add title'),
        '#default_value' => theme_get_setting('footer_middle_section_title_name'),
    ]; 
    }  
    $form['footer_middle_section']['footer_middle_section_description'] = [
        '#type' => 'details',
        '#title' => t('Description section'),
    ];
     
    $form['footer_middle_section']['footer_middle_section_description']['select_footer_middle_section_description'] = [
        '#type' => 'checkbox',
        '#title' => t('You want to display description'),
        '#default_value' => theme_get_setting('select_footer_middle_section_description'),
    ];
    $visible = theme_get_setting('select_footer_middle_section_description');
    if($visible){
    $form['footer_middle_section']['footer_middle_section_description']['footer_middle_section_description_1'] = [
        '#type' => 'textarea',
        '#title' => t('You can add Description_1'),
        '#default_value' => theme_get_setting('footer_middle_section_description_1'),
    ];
    }
    }


    
    // }
//     // Footer Right Section 
    $form['footer_right_section'] = [
        '#type' => 'details',
        '#title' => t('Footer right section'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
    ];
    $form['footer_right_section']['select_footer_right_section'] = [
        '#type' => 'checkbox',
        '#title' => t('You want to add footer right section'),
        '#default_value' => theme_get_setting('select_footer_right_section'),
    ];
    $visible = theme_get_setting('select_footer_right_section');
    if($visible){
        $form['footer_right_section']['footer_right_section_title'] = [
            '#type' => 'details',
            '#title' => t('Title section'),
        ];
        $form['footer_right_section']['footer_right_section_title']['select_footer_right_section_title'] = [
            '#type' => 'checkbox',
            '#title' => t('You want to add title'),
            '#default_value' => theme_get_setting('select_footer_right_section_title'),
        ];
        $visible = theme_get_setting('select_footer_right_section_title');
        if($visible){
         $form['footer_right_section']['footer_right_section_title']['footer_right_section_title_name'] = [
        '#type' => 'textfield',
        '#title' => t('You can add title'),
        '#default_value' => theme_get_setting('footer_right_section_title_name'),
        ];
        }
        $form['footer_right_section']['footer_right_section_description'] = [
            '#type' => 'details',
            '#title' => t('Description section'),
        ];
        $form['footer_right_section']['footer_right_section_description']['select_footer_right_section_description'] = [
            '#type' => 'checkbox',
            '#title' => t('You want to add description'),
            '#default_value' => theme_get_setting('select_footer_right_section_description'),
        ];
        $visible = theme_get_setting('select_footer_right_section_description');
        if($visible){
         $form['footer_right_section']['footer_right_section_description']['footer_right_section_description_text'] = [
        '#type' => 'textarea',
        '#title' => t('You can add description'),
        '#default_value' => theme_get_setting('footer_right_section_description_text'),
        ];
        }
    //Social Media icon in footer right Section
    $form['footer_right_section']['show_social_icon'] = [
        '#type' => 'details',
        '#title' => t('Social Media Link'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
    ];
    $form['footer_right_section']['show_social_icon']['check_social_icon'] = [
        '#type' => 'checkbox',
        '#title' => t('Display Social Icons'),
        '#default_value' => theme_get_setting('check_social_icon'),
    ];
    $show = theme_get_setting('check_social_icon');
    if($show){
         $form['footer_right_section']['show_social_icon']['facebook_url'] = [
        '#type' => 'textfield',
        '#title' => t('Facebook Link'),
        '#default_value' => theme_get_setting('facebook_url'),
        ];
    
    $form['footer_right_section']['show_social_icon']['twitter_url'] = [
        '#type' => 'textfield',
        '#title' => t('Twitter Link'),
        '#default_value' => theme_get_setting('twitter_url'),
    ];
    $form['footer_right_section']['show_social_icon']['youtube_url'] = [
        '#type' => 'textfield',
        '#title' => t('Instagram Link'),
        '#default_value' => theme_get_setting('youtube_url'),
    ];
    $form['footer_right_section']['show_social_icon']['linkedin_url'] = [
        '#type' => 'textfield',
        '#title' => t('Linkedin Link'),
        '#default_value' => theme_get_setting('linkedin_url'),
    ];
    }
    }
     // Copyright 
     $form['copyright'] = [
        '#type' => 'details',
        '#title' => t('Footer copyright section'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
    ];
    $form['copyright']['select_copyright'] = [
        '#type' => 'checkbox',
        '#title' => t('You want to add developer name'),
        '#default_value' => theme_get_setting('select_copyright'),
    ];
    $display = theme_get_setting('select_copyright');
    if ($display){
         $form['copyright']['copyright_text'] = [
        '#type' => 'textarea',
        '#title' => t('Write text with developer name'),
        '#default_value' => theme_get_setting('copyright_text'),
      ];
    }
    
}
 
}